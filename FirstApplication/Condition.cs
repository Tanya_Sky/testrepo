﻿using System;
using System.Linq;

namespace FirstApplication
{
    public static class Condition
    {
        /// <summary>
        /// Implement code according to description of  task 1
        /// </summary>        
        public static int Task1(int n)
        {
            int x;
            if (n > 0)
            {
                x = n * n;
            }
            else 
            {
                if (n<0)
                {
                    x = Math.Abs(n);
                }
                else
                {
                    x = 0;
                }
            }
            return x;
        }

        /// <summary>
        /// Implement code according to description of  task 2
        /// </summary>  
        public static int Task2(int n)
        {
            //int maxNumber = int.Parse(new string(number.ToString().OrderByDescending(x => x).ToArray()));
            int result = 0;
            if (n >= 100 && n <= 999)
            {
                result = Convert.ToInt32(new string(n.ToString().OrderByDescending(x => x).ToArray()));
            }

                return result;

        }
    }

}
