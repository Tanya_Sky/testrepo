﻿using System;

namespace FirstApplication
{
    public static class Arrays
    {
        public static int[] Task1(int[] mas1, int[] mas2)
        {
            for (int i = 0; i < (5); i++)
            {
                if ((mas1[0 + i] % 2) == 0 && (mas1[9 - i] % 2) == 0)
                {
                    mas2[0 + i] = mas1[9 - i];
                    mas2[9 - i] = mas1[0 + i];
                }
                else
                {
                    mas2[0 + i] = mas1[0 + i];
                    mas2[9 - i] = mas1[9 - i];
                }

            }
            return mas2;
        }

        public static int Task2(int[] nums2, int distance)
        {
            int max1 = nums2[0];
            int index1 = 0;
            int index2 = 0;
            for (int i = 0; i < nums2.Length; i++)
            {
                if (max1 <= nums2[i])
                {
                    max1 = nums2[i];
                    index1 = i;
                }
            }
                for (int i = 0; i < nums2.Length; i++)
                {
                    if (nums2[i] == max1)
                    {

                        index2 = i;
                    break;
                    }

                }
            distance = index1 - index2;
                 Console.WriteLine("Расстояние", distance);
            return distance;

        }
        public static int[,] Task3(int[,] mas3, int[,] mas4)
        {
            for (int i = 0; i < mas3.GetLength(0); i++)
                for (int j = 0; j < mas3.GetLength(1); j++)
                    if (i == j)
                        mas4[i, j] = mas3[i,j];
                    else
                    {
                        if (i < j)
                            mas4[i, j] = 1;
                        else
                            mas4[i, j] = 0;
                    }
                    
                    return mas4;
        }
        // for (int i=0; i<10; i++)
        // for (int j=i+1; j<10; j++)
        // if (m[i]==m[j]);
        // return m[i];

        // for (int i=0; i<9; i++)
        // if (m[i+1]==m[i])

        //return m.sum()-45;
    }
}