﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApplication
{
    class Person
    {
        public string firstname;
        public string lastname;
        public int age;
    }

    class ClassesLecture
    {
        static void Lecture5(string[] args)
        {
            Person p1 = new Person(); // конструктор
            p1.firstname = "Ira";
            p1.lastname = "Bohuta";
            p1.age = 19;

            Console.WriteLine($"Firstname:{p1.firstname} Lastname:{p1.lastname} Age:{p1.age} years");

            Person p2 = new Person();
            p2.firstname = "Anna";
            Console.WriteLine($"Firstname:{p2.firstname} Lastname:{p2.lastname} Age:{p2.age} years");

            Person[] persons = new Person[3];
            persons[0] = p1;
            persons[1] = p2;
            foreach (Person person in persons)
                if (person != null)
                    Console.WriteLine(person.firstname);
        }
    }
    class Person2
    {
        private string firstname;
        private string lastname;
        private int age;
        // Method for setting the value of the "firstname" field
        public void SetFirstName(string firstname) //указываем метод 
        {
            this.firstname = firstname; // имени поля присваиваем параметр; this указывает на параметр текущего класса
        }

    }
}

   
