﻿using FirstApplication;
using System;

namespace FirstApplication
{
    public static class LoopTasks
    {
        /// <summary>
        /// Task 1
        /// </summary>
        public static int Task1(int n)
        {
            //this method should return the sum of digits of n.
            //int n = int.Parse(Console.ReadLine());
            n = Math.Abs(n);
            int sum = 0;
            if (n < Math.Pow(10,9))
            {
                while (n > 0)
                {
                    sum = sum + n % 10;
                    n = n / 10;
                }
            }
            return sum;
        }

        /// <summary>
        /// Task 2
        /// </summary>
        public static int Task2(long n)
        {
            //this method should return the number of char '1' in the binary notation of n.
            n = Math.Abs(n);
            long sum = 0;
            if (n < Math.Pow(10, 18))
            {
                while (n > 0)
                {
                    sum = sum + n % 2;
                    n = n / 2;
                }
            }
            return (int)sum;
        }


        /// <summary>
        /// Task 3 
        /// </summary>
        public static int Task3(int n)
        {
            //this method should return the sum of the first n Fibonacci numbers.
            int result = 0;
            int n0 = 0;
            int n1 = 1;
            int next;
            for (int step =0; step<n; step++)
            {
                result = result + n0;
                next = n0 + n1;
                n0 = n1;
                n1 = next;
            }
            return result;
        }
    }
}