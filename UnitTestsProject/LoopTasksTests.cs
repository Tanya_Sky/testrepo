using NUnit.Framework;
using FirstApplication;

namespace UnitTestsProject
{
    [TestFixture]
    public class LoopTasksTests
    {
        [TestCase(1, 1)]
        [TestCase(2, 2)]
        [TestCase(1234, 10)]
        [TestCase(-246, 12)]
	[TestCase(999999999, 81)]
        public void Task1_ReturnsCorrectValue(int n, int expected)
        {
            var actual = LoopTasks.Task1(n);

            Assert.AreEqual(expected, actual, "Task1 returns incorrect value.");
        }

        [TestCase(14, 3)]
        [TestCase(128, 1)]
        [TestCase(0, 0)]
        [TestCase(72057594037927936, 1)]
        [TestCase(72057594037927935, 56)]
        [TestCase(1, 1)]
        [TestCase(5, 2)]
        public void Task2_ReturnsCorrectValue(long n, int expected)
        {
            var actual = LoopTasks.Task2(n);
            
            Assert.AreEqual(expected, actual, "Task2 returns incorrect value.");
        }
        
        [TestCase(7, 20)]
        [TestCase(8, 33)]
        [TestCase(9, 54)]
        [TestCase(10, 88)]
        [TestCase(11, 143)]
	[TestCase(45, 1836311902)]
        [TestCase(1, 0)]
        [TestCase(2, 1)]
        public void Task3_ReturnsCorrectValue(int n, int expected)
        {
            var actual = LoopTasks.Task3(n);
            
            Assert.AreEqual(expected, actual, "Task3 returns incorrect value.");
        }
    }
}