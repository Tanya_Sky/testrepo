using NUnit.Framework;
using FirstApplication;
using System;
using System.Collections.Generic;
using System.Linq;


namespace UnitTestsProject
{

    [TestFixture]
    public class ArrayTasksTest
    {
        [Test]
        public void Task1_Test()
        {
            test(new int[] { 20, 15, -15, 0 }, new int[] { 0, 15, -15, 20 });
            test(new int[] { 11, 20, 30, 40, 55 }, new int[] { 11, 40, 30, 20, 55 });
            test(new int[] { 11, 20, 33, 40, 11, 55 }, new int[] { 11, 20, 33, 40, 11, 55 });
            test(new int[] { 4, -6, 8, 10, -12, -2 }, new int[] { -2, -12, 10, 8, -6, 4 });
            test(new int[] { 11, 33, 55, 77 }, new int[] { 11, 33, 55, 77 });
            test(new int[] { 1 }, new int[] { 1 });
            test(new int[] { }, new int[] { });

            void test(int[] mas1, int[] mas2)
            {
                Arrays.Task1(mas1, mas2);
                //Assert.AreEqual(mas2, mas1,
                //    "Task1 worked incorrectly. Check your solution and change it.");
            }
        }

        [Test]
        public void Tas2_Test()
        {
            //test2(new int[] { 4, 3, 100, 4 }, 0);
            //test2(new int[] { -5, 4, 50, 50, -5 }, 1);
            //test2(new int[] { 100, 350, 350, 100, 350, 100 }, 3);
            //test2(new int[] { 10, 10, 10, 10, 10 }, 4);
            //test2(new int[] { -70, -50, -30, -10, -15 }, 0);
            //test2(new int[] { 13 }, 0);
            //test2(new int[] { }, 0);

            //void test2(int[] nums, int expectedResult)
            //{
            //    int actualResult = ArrayTasks.Task2(nums);
            //    Assert.AreEqual(expectedResult, actualResult,
            //        "Task2 worked incorrectly. Check your solution and change it.");
            //}
        }

        [Test]
        public void Task3_Test()
        {
            //test3(new int[,] { { 1, 0, 0 }, { 1, 1, 0 }, { 1, 1, 1 } },
            //    new int[,] { { 1, 1, 1 }, { 0, 1, 1 }, { 0, 0, 1 } });
            //test3(new int[,] { { 2, 4, 3, 3 }, { 5, 7, 8, 5 }, { 2, 4, 3, 3 }, { 5, 7, 8, 5 } },
            //    new int[,] { { 2, 1, 1, 1 }, { 0, 7, 1, 1 }, { 0, 0, 3, 1 }, { 0, 0, 0, 5 } });
            //test3(new int[,] { { 10, -5 }, { -5, -15 } }, new int[,] { { 10, 1 }, { 0, -15 } });

            //void test3(int[,] matrix, int[,] expectedResult)
            //{
            //    ArrayTasks.Task3(matrix);
            //    Assert.AreEqual(expectedResult, matrix,
            //        "Task3 worked incorrectly. Check your solution and change it.");
            //}
        }
    }


}